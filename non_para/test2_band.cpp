#include <iostream>
#include <cmath>
using namespace std; 
#include "cblas.h"

#define F77NAME(x) x##_ 
extern "C" { 
// Performs LU factorisation of general banded matrix
void F77NAME(dgbtrf)	(const int& N, const int& M, const int& KL, 
                        const int& KU, double* AB, const int& LDAB,
                        int* IPIV, int* INFO);

// Solves pre-factored system of equations
void F77NAME(dgbtrs)	(const char& TRANS, const int& N, const int& KL, 
                        const int& KU,
                        const int& NRHS, double* AB, const int& LDAB,
                        int* IPIV, double* B, const int& LDB, int* INFO);

// LAPACK routine for solving 2 norm of a vector
double F77NAME(dnrm2)	(const int& n, const double *x, 
			const int& incx);
 
} 


//print Matrix s v u or w
void Print_M		(int Nx, int Ny, double* s) {

    for (int j = Ny-1; j > -1; j--){
	for (int i = 0; i < Nx; i++){
	    cout.width(10);cout.precision(3);
	    cout << s[i*Ny+j] << '\t' ; 
	} 
	cout << endl;
    } 
    cout << "-----------------------------------------" <<endl;
}
    

//delta t restriction check
void CFL_check		(double dt, double Re, double dx, double dy) {

    if (dt >= Re*dx*dy/4.0) {
	cout << "-------   Warning: too large dt   -------" << endl
	     << "------- LOL you code will blow up -------"<<endl 
	     << "-----------------------------------------" <<endl;
	}
}

//construct A matrix
void Construct_A 	(double p1,double p2, double p3, 
			int Nx, int Ny, int n1, 
			int kl, int ku, int ldab, int* ipiv, double* A ) {
    int info;

    for (int i = 0; i < Nx-2; i++){
	for (int j = 0; j < Ny-2; j++){
	    A[(i*(Ny-2)+j)*ldab+(kl)] = p2;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j+1)*ldab+(kl+ku-1)] = p3;
	    }			

	    A[(i*(Ny-2)+j)*ldab+(kl+ku)] = p1;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j)*ldab+(kl+ku+1)] = p3;
	    }			
			
	    A[(i*(Ny-2)+j)*ldab+(2*kl+ku)] = p2;				
    	} 
    }

   //print A
    cout << "matrix A:" << endl;
    for (int i = 0; i <ldab; ++i){
	for (int j = 0; j < n1; ++j){
	   cout.precision(3);
	    cout << A[j*ldab+i] << '\t' ; 
	} 
	cout << endl;
     }

    F77NAME(dgbtrf)(n1, n1, kl, ku, A, ldab, ipiv, &info);

    if (info) {
        cout << "Failed to LU factorise matrix" << endl;
    }

   //print A
    cout << "matrix A:" << endl;
    for (int i = 0; i <ldab; ++i){
	for (int j = 0; j < n1; ++j){
	   cout.precision(3);
	    cout << A[j*ldab+i] << '\t' ; 
	} 
	cout << endl;
     }

}


//initialization process
void Initial_p		(int Nx, int Ny, 
			double* v, double* s, double* uv, double* vv, 
			double u) {

    //set entire doamin as 0
    for (int i = 0; i < Nx; i++){
	for (int j = 0; j < Ny; j++){
	    v[i*Ny+j] = 0; 
	    s[i*Ny+j] = 0; 
	} 
    }

    //boundary of u and v velocities
    for (int i = 0; i < Nx; i++){   
	vv[i*Ny+Ny-1] = 0;						//top vv
	uv[i*Ny+Ny-1] = u;						//top uv
	vv[i*Ny+0] = 0;							//bottom vv
	uv[i*Ny+0] = 0;							//bottom uv
    }
    for (int j = 1; j < Ny-1; j++){
	vv[j] = 0;							//left vv
	uv[j] = 0;							//left uv
	vv[(Nx-1)*Ny+j] = 0;						//right vv
	uv[(Nx-1)*Ny+j] = 0;						//right uv
    }
}


//boundary conditions at time t
void BC_t 		(int Nx, int Ny, 
			double dx, double dy, double p4, double p5, 
			double* v, double* s, double u) {

    for (int i = 0; i < Nx; i++){
	v[i*Ny+Ny-1] = (s[i*Ny+Ny-1]-s[i*Ny+Ny-2])*2.0/p5-2.0*u/dy;       //top v
	v[i*Ny+0] = (s[i*Ny+0]-s[i*Ny+1])*2.0/p5;			  //bottom v
    }	

    for (int j = 1; j < Ny-1; j++){
	v[j] = (s[j]-s[1*Ny+j])*2.0/p4;  				  //left
	v[(Nx-1)*Ny+j] = (s[(Nx-1)*Ny+j]-s[(Nx-2)*Ny+j])*2.0/p4;	  //right
    }
}


//interior vorticity at time t
void Vor_t 		(int Nx, int Ny, 
			double dx, double dy, double p4, double p5, 
			double* v, double* s) {

    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] = -(s[(i+1)*Ny+j]-2*s[i*Ny+j]+s[(i-1)*Ny+j])/p4
			-(s[i*Ny+(j+1)]-2*s[i*Ny+j]+s[i*Ny+(j-1)])/p5;
	} 
    } 
}


//interior vorticity at time t+dt
void Vor_t_plus 	(int Nx, int Ny, int n,
			double dx, double dy, double p4, double p5, 
			double* v, double* s, double dt, double Re) {

    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] += dt*(
			-(s[i*Ny+j+1]-s[i*Ny+j-1])*(v[(i+1)*Ny+j]-v[(i-1)*Ny+j])/(4*dx*dy)
			+(v[i*Ny+j+1]-v[i*Ny+j-1])*(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(4*dx*dy)
			+1/Re*((v[(i+1)*Ny+j]-2*v[i*Ny+j]+v[(i-1)*Ny+j])/p4
			      +(v[i*Ny+(j+1)]-2*v[i*Ny+j]+v[i*Ny+(j-1)])/p5)
			);
	} 
    }

}


//streamfunction at time t+dt
void Str_t_plus 	(int Nx, int Ny, int n, int n1, int nrhs, int info,
			int kl, int ku, int ldab, 
			double p2, double* A, double* A1, double* v, 
			double* s, double* s1, double* s_sub, int* ipiv) {

    for (int i = 0; i < n; i++){					
    s1[i]= s[i];							//save s at time t
    }

    for (int i = 0; i < Nx-2; i++){					//substract s_sub
	for (int j = 0; j < Ny-2; j++){
	s_sub[i*(Ny-2)+j] = v[(i+1)*Ny+j+1];
	}
    }

    F77NAME(dgbtrs)('N', n1, kl, ku, nrhs, A, ldab, ipiv, s_sub, n1, &info);//solve s at time t+dt

    if (info) {
        cout << "Failed to solve" << endl;
    }

    for (int i = 0; i < Nx-2; i++){					//re_construct s with a new s_sub
	for (int j = 0; j < Ny-2; j++){
	 s[(i+1)*Ny+j+1] = s_sub[i*(Ny-2)+j];
	}
    }
}


//define convergance
int Def_Cov 		(double* s, double* s1 , int n ) {

    double ds=0;								//use to dudge
    for (int i = 0; i < n; i++){
    s1[i]-= s[i];							//calculate s1
    }
    ds = F77NAME(dnrm2) (n, s1, 1);					//calculate ds

    cout << "ds: " << ds << endl ;
 

    if (ds < 0.001) {
	cout << "Congrats ConVerged" <<endl;				//determine DIV
    cout << "-----------------------------------------" <<endl;   
	return 1;
    }

    if (ds > 1000) {
	cout << "LOL U DIverged" <<endl;				//determine DIV
    cout << "-----------------------------------------" <<endl;   
	return 1;
    }

    else {
    cout << "-----------------------------------------" <<endl;   
	return 0;
    }

}

//calculate interial u and v velocity and print
void Cal_UV		 (int Nx, int Ny, double dx, double dy,
			double* s, double* uv, double* vv) {

    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    uv[i*Ny+j] = (s[i*Ny+j+1]-s[i*Ny+j-1])/(2*dy);
	    vv[i*Ny+j] = -(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(2*dx);			
	} 
    }

    cout << "horizontal velocity array:" << endl;  
    Print_M (Nx, Ny, uv);

    cout << "vertical velocity array:" << endl;  
    Print_M (Nx, Ny, vv);
}


int main(int argc, char** argv) 
{ 
    //******************************************************************//Initialize code
    //define parameters
    double Lx = 0;	//Length of the domain in the x-direction
    double Ly = 0;	//Length of the domain in the y-direction
    int    Nx = 0;	//Number of grid points in x-direction.
    int    Ny = 0;	//Number of grid points in y-direction.
    int    Px = 1; 	//Number of partitions in the x-direction (parallel)
    int    Py = 1; 	//Number of partitions in the y-direction (parallel)
    double dt = 0;	//Time step size
    double T  = 0;	//Final time
    double Re = 0;	//Renolds number

    double u  = 1.0;	//U velocity
    double dx = 0;	//Node distence in x direction
    double dy = 0;	//Node distence in y direction
    double t  = 0;	//Current time
    int timestep = 0;	//tiem step

    double p1, p2, p3;	//Used for construct A matrix
    double p4, p5;	//Used for iteration
    int kl, ku, ldab, ldb;
    const int nrhs = 1; //Used for linear agerbra solver
    int  info = 0;	//Used for linear agerbra solver

    int   n   = 0;	//v s vv ss elements number
    int   n1  = 0;	//A matrix dimension		
    double* A = nullptr; 	//A matrix
    double* A1= nullptr; 	//A matrix copy
    int* ipiv = nullptr; 	// Vector for pivots 
    double* v = nullptr; //vorticity
    //double* v1= nullptr; //vorticity from last step
    double* s = nullptr; //streamfunction
    double* s1= nullptr; //streamfunction from last step
    double* s_sub = nullptr; //interia streamfunction
    double* vv= nullptr; //v velocity
    double* uv= nullptr; //u velocity

    //announcement of author and purpose
    cout     << "---------------------------------------- " << endl
	     << "-- HPC CW to solve lid driven problem -- " << endl
	     << "--         Author: Songrui LI	      -- " << endl
	     << "--        CID number: 01742372	      -- " << endl 
	     << "---------------------------------------- " << endl;

    //announce and test parameters entred
    cout << "You have entered " << argc-1 << " arguments:" << endl; 
    if (argc > 10) {
	cout << "-----------------------------------------" <<endl
	     << "-- Warning: too much parameters input --" << endl
	     << "only the first 9 of arguments will be taken "<<endl 
	     << "-----------------------------------------" <<endl;
	}

    //allocate parameters
    Lx = stod(argv[1]);
    Ly = stod(argv[2]);
    Nx = stoi(argv[3]);
    Ny = stoi(argv[4]);
    Px = stoi(argv[5]);
    Py = stoi(argv[6]);
    dt = stod(argv[7]);
    T  = stod(argv[8]);
    Re = stod(argv[9]);

    //list parameters
    cout << "Lx = " << Lx <<endl;
    cout << "Ly = " << Ly <<endl;
    cout << "Nx = " << Nx <<endl;
    cout << "Ny = " << Ny <<endl;
    cout << "Px = " << Px <<endl;
    cout << "Py = " << Py <<endl;
    cout << "dt = " << dt <<endl;
    cout << "T  = " << T  <<endl;
    cout << "Re = " << Re <<endl;
    cout << "-----------------------------------------" <<endl;

    //******************************************************************//preparing
    //prior calcution
    dx = Lx/(Nx-1);
    dy = Ly/(Ny-1);
    n = Nx*Ny;
    n1 = (Nx-2)*(Ny-2);
    CFL_check(dt, Re, dx, dy);				//check the CFL number
    p4 = dx*dx;
    p5 = dy*dy;
    p1 = 2/p4+2/p5;
    p2 = -1/p5;
    p3 = -1/p4;

    kl = Ny-2;
    ku = Ny-2;
    ldab = 1+2*kl+ku;

    //prior set pointers
    v = new double[n];
    //v1= new double[n];
    s = new double[n];
    s1= new double[n];
    s_sub = new double[n1];
    A = new double[ldab*n1];
    A1 = new double[ldab*n1];
    ipiv = new int [n1];
    vv= new double[n];
    uv= new double[n];

    Construct_A(p1, p2, p3, Nx, Ny, n1, kl, ku, ldab, ipiv, A); 		//construct A matrix for linear system

    Initial_p(Nx,Ny,v,s,uv,vv,u);   			//initialization



    //******************************************************************//iteration Process

    //first step

    cout << "timestep: " << ++timestep << endl;   
    cout << "-----------------------------------------" <<endl;
    t += dt;

    BC_t (Nx, Ny, dx, dy, p4, p5, v, s, u);
    Vor_t (Nx, Ny, dx, dy, p4, p5, v, s);
    Vor_t_plus (Nx, Ny, n1, dx, dy, p4, p5, v, s, dt, Re);

    cout<<"v"<<endl;
    Print_M(Nx,Ny,v);

    cout<<"s"<<endl;
    Print_M(Nx,Ny,s);

    Str_t_plus (Nx, Ny, n, n1, nrhs, info, kl, ku, ldab, p2, A, A1, v, s, s1, s_sub, ipiv);

    cout<<"s"<<endl;
    Print_M(Nx,Ny,s);

    Def_Cov(s, s1, n);

    for (; t<T;) {
    cout << "timestep: " << ++timestep << endl;   
    cout << "-----------------------------------------" <<endl;
    t += dt;

    BC_t (Nx, Ny, dx, dy, p4, p5, v, s, u);

    //cout<<"v"<<endl;
    //Print_M(Nx,Ny,v);

    Vor_t_plus (Nx, Ny, n1, dx, dy, p4, p5, v, s, dt, Re);

    cout<<"v"<<endl;
    Print_M(Nx,Ny,v);

    Str_t_plus (Nx, Ny, n, n1, nrhs, info, kl, ku, ldab, p2, A, A1, v, s, s1, s_sub, ipiv);

    cout<<"s"<<endl;
    Print_M(Nx,Ny,s);

    if (Def_Cov(s, s1, n)) {break;}			//define convergence and divergence    
    
    //Cal_UV (Nx, Ny, dx, dy,s, uv, vv);			//calculate interial u and v velocities 

    }

    Cal_UV (Nx, Ny, dx, dy,s, uv, vv);			//calculate interial u and v velocities 

    //******************************************************************//release memory
    delete [] v;
    //delete [] v1;
    delete [] s1;
    delete [] s;
    delete [] s_sub;
    delete [] A;
    delete [] A1;
    delete [] ipiv;
    delete [] vv;
    delete [] uv;

    return 0; 
} 
