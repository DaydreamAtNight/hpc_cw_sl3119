#include <iostream>
#include <string>
#include <cmath>
using namespace std; 

#define F77NAME(x) x##_ 
extern "C" { 
// Performs LU factorisation of general banded matrix
void F77NAME(dgbtrf)	(const int& N, const int& M, const int& KL, 
                        const int& KU, double* AB, const int& LDAB,
                        int* IPIV, int* INFO);

// Solves pre-factored system of equations
void F77NAME(dgbtrs)	(const char& TRANS, const int& N, const int& KL, 
                        const int& KU,
                        const int& NRHS, double* AB, const int& LDAB,
                        int* IPIV, double* B, const int& LDB, int* INFO);

// LAPACK routine for solving 2 norm of a vector
double F77NAME(dnrm2)	(const int& n, const double *x, 
			const int& incx);
 
} 

//--------------------------------------------------------------------------------//
//--------------------------------DEFINE CLASSES----------------------------------//
//--------------------------------------------------------------------------------//

// Define class LidDrivenCavity---------------------------------------------------//
class LidDrivenCavity
{
public:
    LidDrivenCavity();
    ~LidDrivenCavity();
    
    //set parameters
    void SetDomainSize(double xlen, double ylen);
    void SetGridSize(int nx, int ny);
    void SetTimeStep(double deltat);
    void SetReynoldsNumber(double Re);

    //Print reults
    void Print_v();
    void Print_s();
    void Print_uv();
    void Print_vv();

    //Calculate
    void Prior_Cal();		//Prior Calculation
    int CFL_Check();		//ckeck the CFL number
    void Construct_A();		//Construct A matrix and imply pre_factorazation

    void Initialise();		//initialization process
    void BC_t();		//boundary conditions at time t
    void Vor_t();		//interior vorticity at time t
    void Vor_t_plus();		//interior vorticity at time t+dt
    void Str_t_plus();		//streamfunction at time t+dt

    int Def_Cov();		//define convergance
    void Cal_UV();		//calculate interial u and v velocity and print

private:
    double Lx = 0;		//Length of the domain in the x-direction
    double Ly = 0;		//Length of the domain in the y-direction
    int    Nx = 0;		//Number of grid points in x-direction.
    int    Ny = 0;		//Number of grid points in y-direction.
    int    Px = 1; 		//Number of partitions in the x-direction (parallel)
    int    Py = 1; 		//Number of partitions in the y-direction (parallel)
    double dt = 0;		//Time step size
    //double T  = 0;		//Final time
    double Re = 0;		//Renolds number

    double u  = 1.0;		//U velocity
    double dx = 0;		//Node distence in x direction
    double dy = 0;		//Node distence in y direction
    double t  = 0;		//Current time
    int timestep = 0;		//tiem step
	
    double p1, p2, p3;		//Used for construct A matrix
    double p4, p5;		//Used for iteration
    int kl, ku, ldab, ldb;
    const int nrhs = 1; 	//Used for linear agerbra solver
    int  info = 0;		//Used for linear agerbra solver

    int   n   = 0;		//v s vv ss elements number
    int   n1  = 0;		//A matrix dimension
		
    double* A = nullptr; 	//A matrix
    int* ipiv = nullptr; 	// Vector for pivots 
    double* v = nullptr; 	//vorticity
    double* s = nullptr; 	//streamfunction
    double* s1= nullptr; 	//streamfunction from last step
    double* s_sub = nullptr; 	//interia streamfunction
    double* vv= nullptr; 	//v velocity
    double* uv= nullptr; 	//u velocity
    
};

//define class Poisson ---------------------------------------------------------//
class PoissonSolver
{
public:

    PoissonSolver();
    ~PoissonSolver();

    void Get_para(int n11, int kl1, int ku1, int ldab1,	
	          int* ipiv1, double* A1, double* s_sub1);	//Pass parameters from Lid class
    void Solve_L();						//Solve linear algerbra
    double* Give_para();					//give para to Lid class
	
private:
    int     n1   = 0;
    int     kl   = 0;
    int     ku   = 0;
    int     ldab = 0;
    int     nrhs = 1;
    int     info = 0;
    int*    ipiv = nullptr;
    double* A    = nullptr;
    double* s_sub= nullptr;

};

//--------------------------------------------------------------------------------//
//--------------------------------DEFINE FUNCTIONS--------------------------------//
//--------------------------------------------------------------------------------//

//tool function: print Matrix s v u or w -----------------------------------------//
void Print_M(int Nx, int Ny, double* s) {

    for (int j = Ny-1; j > -1; j--){
	for (int i = 0; i < Nx; i++){
	    cout.width(10);cout.precision(3);
	    cout << s[i*Ny+j] << '\t' ; 
	} 
	cout << endl;
    } 
    cout << "-----------------------------------------" <<endl;
}


// Member functions & constructors for LidDrivenCavity ---------------------------//

LidDrivenCavity::LidDrivenCavity() {}

LidDrivenCavity::~LidDrivenCavity() {}

void LidDrivenCavity::SetDomainSize(double xlen, double ylen) 
{
    Lx = xlen;
    Ly = ylen;
}

void LidDrivenCavity::SetGridSize(int nx, int ny)
{
    Nx = nx;
    Ny = ny;
}

void LidDrivenCavity::SetTimeStep(double deltat)
{
    dt = deltat;
}


void LidDrivenCavity::SetReynoldsNumber(double re)
{
    Re = re;
}

void LidDrivenCavity::Print_v()		
{
    cout<<"Vorticity array:"<<endl;
    Print_M(Nx,Ny,v);
}

void LidDrivenCavity::Print_s()
{
    cout<<"Stream function array:"<<endl;
    Print_M(Nx,Ny,s);
}

void LidDrivenCavity::Print_uv()
{
    cout << "horizontal velocity array:" << endl;  
    Print_M (Nx, Ny, uv);
}

void LidDrivenCavity::Print_vv()
{
    cout << "vertical velocity array:" << endl;  
    Print_M (Nx, Ny, vv);
}

void LidDrivenCavity::Prior_Cal()	//Prior Calculation
{
    dx = Lx/(Nx-1);
    dy = Ly/(Ny-1);
    n = Nx*Ny;
    n1 = (Nx-2)*(Ny-2);
    p4 = dx*dx;
    p5 = dy*dy;
    p1 = 2/p4+2/p5;
    p2 = -1/p5;
    p3 = -1/p4;

    kl = Ny-2;
    ku = Ny-2;
    ldab = 1+2*kl+ku;

    //prior set pointers
    v = new double[n];
    s = new double[n];
    s1= new double[n];
    s_sub = new double[n1];
    A = new double[ldab*n1];
    ipiv = new int [n1];
    vv= new double[n];
    uv= new double[n];
}

int LidDrivenCavity::CFL_Check()	//Check the CFL number
{
    if (dt >= Re*dx*dy/4.0) {
	cout << "-------   Warning: too large dt   -------" << endl
	     << "------- LOL you code will blow up -------"<<endl 
	     << "-----------------------------------------" <<endl;
    return 1;
	}
    else {
	cout << "----------   CLF check passed   ----------" << endl
	     << "-----------------------------------------" <<endl;
    return 0;
        }
}

void LidDrivenCavity::Construct_A()	//Construct A matrix and imply pre_factorazation
{
    int info;

    for (int i = 0; i < Nx-2; i++){
	for (int j = 0; j < Ny-2; j++){
	    A[(i*(Ny-2)+j)*ldab+(kl)] = p2;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j+1)*ldab+(kl+ku-1)] = p3;
	    }			

	    A[(i*(Ny-2)+j)*ldab+(kl+ku)] = p1;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j)*ldab+(kl+ku+1)] = p3;
	    }			
			
	    A[(i*(Ny-2)+j)*ldab+(2*kl+ku)] = p2;				
    	} 
    }

    F77NAME(dgbtrf)(n1, n1, kl, ku, A, ldab, ipiv, &info);

    if (info) {
        cout << "Failed to LU factorise matrix" << endl;
    }

   /*//print A
    cout << "matrix A:" << endl;
    for (int i = 0; i <ldab; ++i){
	for (int j = 0; j < n1; ++j){
	   cout.precision(3);
	    cout << A[j*ldab+i] << '\t' ; 
	} 
	cout << endl;
     }*/
}

void LidDrivenCavity::Initialise()	//initialization process
{
    //set entire doamin as 0
    for (int i = 0; i < Nx; i++){
	for (int j = 0; j < Ny; j++){
	    v[i*Ny+j] = 0; 
	    s[i*Ny+j] = 0; 
	} 
    }

    //boundary of u and v velocities
    for (int i = 0; i < Nx; i++){   
	vv[i*Ny+Ny-1] = 0;						//top vv
	uv[i*Ny+Ny-1] = u;						//top uv
	vv[i*Ny+0] = 0;							//bottom vv
	uv[i*Ny+0] = 0;							//bottom uv
    }
    for (int j = 1; j < Ny-1; j++){
	vv[j] = 0;							//left vv
	uv[j] = 0;							//left uv
	vv[(Nx-1)*Ny+j] = 0;						//right vv
	uv[(Nx-1)*Ny+j] = 0;						//right uv
    }
}

void LidDrivenCavity::BC_t()		//boundary conditions at time t
{
    for (int i = 0; i < Nx; i++){
	v[i*Ny+Ny-1] = (s[i*Ny+Ny-1]-s[i*Ny+Ny-2])*2.0/p5-2.0*u/dy;       //top v
	v[i*Ny+0] = (s[i*Ny+0]-s[i*Ny+1])*2.0/p5;			  //bottom v
    }	

    for (int j = 1; j < Ny-1; j++){
	v[j] = (s[j]-s[1*Ny+j])*2.0/p4;  				  //left
	v[(Nx-1)*Ny+j] = (s[(Nx-1)*Ny+j]-s[(Nx-2)*Ny+j])*2.0/p4;	  //right
    }
}

void LidDrivenCavity::Vor_t()		//interior vorticity at time t
{
    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] = -(s[(i+1)*Ny+j]-2*s[i*Ny+j]+s[(i-1)*Ny+j])/p4
			-(s[i*Ny+(j+1)]-2*s[i*Ny+j]+s[i*Ny+(j-1)])/p5;
	} 
    } 
}

void LidDrivenCavity::Vor_t_plus()	//interior vorticity at time t+dt	
{
    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] += dt*(
			-(s[i*Ny+j+1]-s[i*Ny+j-1])*(v[(i+1)*Ny+j]-v[(i-1)*Ny+j])/(4*dx*dy)
			+(v[i*Ny+j+1]-v[i*Ny+j-1])*(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(4*dx*dy)
			+1/Re*((v[(i+1)*Ny+j]-2*v[i*Ny+j]+v[(i-1)*Ny+j])/p4
			      +(v[i*Ny+(j+1)]-2*v[i*Ny+j]+v[i*Ny+(j-1)])/p5)
			);
	} 
    }

}

void LidDrivenCavity::Str_t_plus()	//streamfunction at time t+dt	
{
    PoissonSolver* Psolver = new PoissonSolver();		//class PoissonSolver

    for (int i = 0; i < n; i++){					
    s1[i]= s[i];						//save s at time t
    }

    for (int i = 0; i < Nx-2; i++){				//substract s_sub
	for (int j = 0; j < Ny-2; j++){
	s_sub[i*(Ny-2)+j] = v[(i+1)*Ny+j+1];
	}
    }

    Psolver->Get_para(n1, kl, ku, ldab, ipiv, A, s_sub);	//>> Psolver
    Psolver->Solve_L();						//./Psolver
    s_sub = Psolver->Give_para();				//<< Psolver

    for (int i = 0; i < Nx-2; i++){				//re_construct s with a new s_sub
	for (int j = 0; j < Ny-2; j++){
	 s[(i+1)*Ny+j+1] = s_sub[i*(Ny-2)+j];
	if (j == Ny-3) {s_sub[i*(Ny-2)+j] -= s[(i+1)*Ny+j+2]*p2;} //boundary condition
	if (j == 0)    {s_sub[i*(Ny-2)+j] -= s[(i+1)*Ny+j]*p2;}
	if (i == Ny-3) {s_sub[i*(Ny-2)+j] -= s[(i+2)*Ny+j+1]*p2;}
	if (i == 0)    {s_sub[i*(Ny-2)+j] -= s[(i+0)*Ny+j+1]*p2;}
	}
    }

}

int LidDrivenCavity::Def_Cov()		//define convergance
{
    double ds=0;						//use to judge
    for (int i = 0; i < n; i++){
    s1[i]-= s[i];						//calculate s1
    }
    ds = F77NAME(dnrm2) (n, s1, 1);				//calculate ds

    cout << "ds: " << ds << endl ;
 

    if (ds < 0.001) {
	cout << "Congrats ConVerged" <<endl;			//determine DIV
    cout << "-----------------------------------------" <<endl;   
	return 1;
    }

    if (ds > 1000) {
	cout << "LOL U DIverged" <<endl;			//determine DIV
    cout << "-----------------------------------------" <<endl;   
	return 1;
    }

    else {
    cout << "-----------------------------------------" <<endl;   
	return 0;
    }

}

void LidDrivenCavity::Cal_UV()		//calculate interial u and v velocity and print
{
    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    uv[i*Ny+j] = (s[i*Ny+j+1]-s[i*Ny+j-1])/(2*dy);
	    vv[i*Ny+j] = -(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(2*dx);			
	} 
    }

    cout << "horizontal velocity array:" << endl;  
    //Print_M (Nx, Ny, uv);

    cout << "vertical velocity array:" << endl;  
    //Print_M (Nx, Ny, vv);
}


// Member functions & constructors for class PoissonSolver ---------------------------//

PoissonSolver::PoissonSolver() {}

PoissonSolver::~PoissonSolver() {}

void PoissonSolver::Get_para(int n11, int kl1, int ku1, int ldab1,	
	          	     int* ipiv1, double* A1, double* s_sub1)	//Pass parameters from LidDrivenCavity class
{
    n1   = n11;
    kl   = kl1;
    ku   = ku1;
    ldab = ldab1;

    s_sub= new double[n1];
    A = new double[ldab*n1];
    ipiv = new int[n1];

    for(int i=0; i< ldab*n1; i++) {
	A[i] = A1[i];
    }

    for(int i=0; i< n1; i++) {
	ipiv[i] = ipiv1[i];
	s_sub[i] = s_sub1[i];
     }

}

void PoissonSolver::Solve_L()		//Solve poisson function with pre_factorization
{
    F77NAME(dgbtrs)('N', n1, kl, ku, nrhs, A, ldab, ipiv, s_sub, n1, &info); //solve s at time t+dt

    if (info) {
        cout << "Failed to solve" << endl;
    }
}

double* PoissonSolver::Give_para()		//Solve poisson function with pre_factorization
{
    return s_sub;
}

//--------------------------------------------------------------------------------//
//---------------------------------MAIN FUNCTION----------------------------------//
//--------------------------------------------------------------------------------//

int main(int argc, char** argv) 
{ 
    //define parameters
    double Lx = 0;	//Length of the domain in the x-direction
    double Ly = 0;	//Length of the domain in the y-direction
    int    Nx = 0;	//Number of grid points in x-direction.
    int    Ny = 0;	//Number of grid points in y-direction.
    int    Px = 1; 	//Number of partitions in the x-direction (parallel)
    int    Py = 1; 	//Number of partitions in the y-direction (parallel)
    double dt = 0;	//Time step size
    double T  = 0;	//Final time
    double Re = 0;	//Renolds number

    double t  = 0;	//Current time
    int timestep = 0;	//tiem step

    // Create a new instance of the LidDrivenCavity class
    LidDrivenCavity* solver = new LidDrivenCavity();

    //announcement of author and purpose
    cout     << "---------------------------------------- " << endl
	     << "-- HPC CW to solve lid driven problem -- " << endl
	     << "--         Author: Songrui LI	      -- " << endl
	     << "--        CID number: 01742372	      -- " << endl 
	     << "---------------------------------------- " << endl;

    //announce and test the parameters entred
    cout << "You have entered " << argc-1 << " arguments:" << endl; 
    if (argc > 10) {
	cout << "-----------------------------------------" <<endl
	     << "-- Warning: too much parameters input --" << endl
	     << "only the first 9 of arguments will be taken "<<endl 
	     << "-----------------------------------------" <<endl;
	}

    //allocate parameters
    Lx = stod(argv[1]);
    Ly = stod(argv[2]);
    Nx = stoi(argv[3]);
    Ny = stoi(argv[4]);
    Px = stoi(argv[5]);
    Py = stoi(argv[6]);
    dt = stod(argv[7]);
    T  = stod(argv[8]);
    Re = stod(argv[9]);

    //list parameters
    cout << "Lx = " << Lx <<endl;
    cout << "Ly = " << Ly <<endl;
    cout << "Nx = " << Nx <<endl;
    cout << "Ny = " << Ny <<endl;
    cout << "Px = " << Px <<endl;
    cout << "Py = " << Py <<endl;
    cout << "dt = " << dt <<endl;
    cout << "T  = " << T  <<endl;
    cout << "Re = " << Re <<endl;
    cout << "-----------------------------------------" <<endl;

    // alocate to class 
    solver->SetDomainSize(Lx, Ly);
    solver->SetGridSize(Nx, Ny);
    solver->SetTimeStep(dt);
    solver->SetReynoldsNumber(Re);

    //preparation
    solver->Prior_Cal();
    if (solver->CFL_Check()) {return 0;}
    solver->Construct_A();

    //start solver
    solver->Initialise();

    //solver->Print_v();
    //solver->Print_s();

    //first step

    cout << "timestep: " << ++timestep << endl;   
    cout << "-----------------------------------------" <<endl;
    t += dt;

    solver->BC_t();
    solver->Vor_t();
    solver->Vor_t_plus();
    solver->Str_t_plus();
    solver->Def_Cov();

    //solver->Print_v();
    //solver->Print_s();

    for (; t<T;) {
    cout << "timestep: " << ++timestep << endl;   
    cout << "-----------------------------------------" <<endl;
    t += dt;
    solver->BC_t();
    solver->Vor_t_plus();
    solver->Str_t_plus();

    //solver->Print_v();
    //solver->Print_s();

    if (solver->Def_Cov()) {break;}

    }

    solver->Print_v();
    solver->Print_s();
    //solver->Cal_UV();

    return 0;
}
