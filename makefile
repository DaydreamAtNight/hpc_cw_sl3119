#! /bin/bash

#Variables
COMP=mpicxx
RUN=mpiexec
CPP=test7_final.cpp
LIB1=-llapack 
LIB2=-lblas 
EXE=test7.out
NCPU=9

#rm data*

$COMP $CPP $LIB1 $LIB2 -o $EXE

#$RUN -np $NCPU ./$EXE 1 1 47 47 3 3 0.01 0.5 100
#$RUN -np $NCPU ./$EXE 1 1 47 47 3 3 0.04 0.5 400
#$RUN -np $NCPU ./$EXE 1 1 47 47 3 3 0.09 0.5 1000
#$RUN -np $NCPU ./$EXE 1 1 47 47 3 3 0.01 0.5 3200
$RUN -np $NCPU ./$EXE 1 1 62 62 3 3 0.005 0.025 100
$RUN -np $NCPU ./$EXE 1 1 62 62 3 3 0.02 0.1 400
$RUN -np $NCPU ./$EXE 1 1 62 62 3 3 0.045 0.225 1000
$RUN -np $NCPU ./$EXE 1 1 62 62 3 3 0.005 0.025 3200

python PLOT_static_v.py data_vv_x100.txt data_vv_x400.txt data_vv_x1000.txt data_vv_x3200.txt 
python PLOT_static_u.py data_uv100.txt data_uv400.txt data_uv1000.txt data_uv3200.txt 

#python PLOT_static_v1.py data_vv_x* 
#python PLOT_static_u1.py data_uv*

gnuplot courbe_loop40.gnu 
