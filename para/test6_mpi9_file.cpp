//--------------------------------------------------------------------------------//
//    	     		HPC CW to solve lid driven problem			  //
//	     			Author: Songrui LI	      			  //
//	     			CID number: 01742372	    			  //
//			Note: only support parallel with 9 processes		  //
//--------------------------------------------------------------------------------//
#include <iostream>
#include <string>
#include <cmath>
#include <mpi.h>
#include <fstream>
#include <algorithm>
using namespace std; 

#define F77NAME(x) x##_ 
extern "C" { 
// Performs LU factorisation of general banded matrix
void F77NAME(dgbtrf)	(const int& N, const int& M, const int& KL, 
                        const int& KU, double* AB, const int& LDAB,
                        int* IPIV, int* INFO);

// Solves pre-factored system of equations
void F77NAME(dgbtrs)	(const char& TRANS, const int& N, const int& KL, 
                        const int& KU,
                        const int& NRHS, double* AB, const int& LDAB,
                        int* IPIV, double* B, const int& LDB, int* INFO);

// LAPACK routine for solving 2 norm of a vector
double F77NAME(dnrm2)	(const int& n, const double *x, 
			const int& incx);
 
} 

//--------------------------------------------------------------------------------//
//--------------------------------DEFINE CLASSES----------------------------------//
//--------------------------------------------------------------------------------//

// Define class LidDrivenCavity---------------------------------------------------//
class LidDrivenCavity
{
public:
    LidDrivenCavity();
    ~LidDrivenCavity();
    
    //set parameters
    void SetDomaindxdy(double dx1, double dy1);
    void SetGridSize(int nx, int ny);
    void SetTimeStep(double deltat);
    void SetReynoldsNumber(double Re);

    //Print reults
    void Combine_s (int rank, int size);	//combine all domains and print out s and v
    void Combine_v (int rank, int size);	//combine all domains and print out uv and vv
    void Find_min  (int rank, int size);	//find the minimum value of s

    //Calculation
    void Prior_Cal();				//Prior Calculation
    void Construct_A();				//Construct A matrix and imply pre_factorazation

    void Initialise();				//initialization process
    void BC_t(int rank);			//boundary conditions at time t
    void Vor_t();				//interior vorticity at time t
    void Vor_t_plus();				//interior vorticity at time t+dt
    void Str_t_plus();				//streamfunction at time t+dt

    //check
    int CFL_Check(int rank);			//check the CFL number
    int Def_Cov(int rank, int size);		//define convergance

    //parallel communication
    void Send_reciv_v (int rank, int size); 	//used to swap data from neighbouring domains
    void Send_reciv_s (int rank, int size); 	//used to swap data from neighbouring domains

private:

    //Input parameters
    double dx = 0;				//Node distence in x direction
    double dy = 0;				//Node distence in y direction
    int    Nx = 0;				//Number of grid points in x-direction.
    int    Ny = 0;				//Number of grid points in y-direction.
    double dt = 0;				//Time step size
    double Re = 0;				//Renolds number
    const double u  = 1.0;			//U velocity

    //Self defined intermediate parameters
    double p1, p2, p3;				//Parameters used for construct A matrix
    double p4, p5;				//Parameters used for iteration

    //Parameters used for linear agerbra solver
    int kl, ku, ldab;				//Parameters used for matrix A
    const int nrhs = 1; 			//parameters used for lapack
    int  info = 0;				//parameters used for lapack

    //Number of data for subdomain and interia subdomain
    int   n   = 0;				//v s elements number
    int   n1  = 0;				//Interia v s matrix dimension

    //Vectors and matrix
    double* A = nullptr; 			//A matrix
    int* ipiv = nullptr; 			//Vector for pivots 
    double* v = nullptr; 			//Vorticity
    double* s = nullptr; 			//Streamfunction
    double* s1= nullptr; 			//Streamfunction from last step
    double* s_sub = nullptr; 			//Interia streamfunction
    
};

//define class Poisson ---------------------------------------------------------//
class PoissonSolver
{
public:

    PoissonSolver();
    ~PoissonSolver();

    void Get_para(int n11, int kl1, int ku1, 	//Grab parameters from Lid class
		  int ldab1, int* ipiv1,		
	          double* A1, double* s_sub1);	
    void Solve_L();				//Solve linear algerbra
    double* Give_para();			//Give para to Lid class
	
private:
    int     n1   = 0;				//Deminsion of A matrix
    int     kl   = 0;				//Lower sub_diagonal
    int     ku   = 0;				//Upper sub_diagonal
    int     ldab = 0;				//Deminsion of A matrix
    int     nrhs = 1;				//Used in lapack routine
    int     info = 0;				//Used in lapack routine
    int*    ipiv = nullptr;			//Vector for pivots 
    double* A    = nullptr;			//A matrix
    double* s_sub= nullptr;			//b vector to be solved
};

//--------------------------------------------------------------------------------//
//--------------------------------DEFINE FUNCTIONS--------------------------------//
//--------------------------------------------------------------------------------//

//Announcements: make an announcement before real calculation
void Announce	(int argc, int rank, int size, 			
		int Lx, int Ly, int Nx, int Ny, 
		int Px, int Py, double dt, double T, double Re)
{
    if (rank == 0) 				
    {
    	//announcement of author and purpose
    	cout << "---------------------------------------- " << endl
	     << "-- HPC CW to solve lid driven problem -- " << endl
	     << "--         Author: Songrui LI	      -- " << endl
	     << "--        CID number: 01742372	      -- " << endl 
	     << "---------------------------------------- " << endl;

    	//announce and test the parameters entred
    	cout << "You have entered " << argc-1 << " arguments:" << endl; 
    	if (argc > 10) {
	    cout << "-----------------------------------------" <<endl
	         << "-- Warning: too much parameters input --" << endl
	         << "only the first 9 of arguments will be taken "<<endl 
	         << "-----------------------------------------" <<endl;
	}

        //list parameters
        cout << "Lx = " << Lx <<endl;
        cout << "Ly = " << Ly <<endl;
        cout << "Nx = " << Nx <<endl;
        cout << "Ny = " << Ny <<endl;
        cout << "Px = " << Px <<endl;
        cout << "Py = " << Py <<endl;
        cout << "dt = " << dt <<endl;
        cout << "T  = " << T  <<endl;
        cout << "Re = " << Re <<endl;
        cout << "-----------------------------------------" <<endl;

        //announce subdomain scale
        cout <<"Size of subdomain is " <<endl;
        cout << "Nx' = " << (Nx-2)/Px+2 <<endl;
        cout << "Ny' = " << (Ny-2)/Py+2 <<endl;
        cout << "-----------------------------------------" <<endl;
    }
}

//Check if number of processes = 9
int Check_P	(int rank, int size, int Px, int Py) 				
{
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);      	//Synchronise processes
    if (size != Px*Py || size != 9) {
	if (rank == 0) {
	cout << "-- Warning: number of processes error  --" << endl
	     << "--- processes size should equals to 9 --- "<<endl 
	     << "-----------------------------------------" <<endl;
	}
	return 1;
    }
    else {
	return 0;
    }
}

//Increase timestep and print
void timestep_p (int rank, int* timestep, double* t, double dt) 	
{			
    if (rank == 0) {
	cout << "timestep: " << ++ *timestep << endl;   
    	cout << "-----------------------------------------" <<endl;
    }
    *t += dt;
}

//----------------------------Contour data functions------------------------------//

//Tiny function(used in Print_M3): Print one new line into file
void Print_endc	(int rank, const char* filename)  
{
    fstream myfile;
    myfile.open (filename, fstream::app);
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
    if (rank == 0) {
    	myfile << endl;
	cout << "-";
    }
    myfile.close();
}

//Tiny funtion: Print one colomn data for one processer into file 
void Print_M	(double* s, int Nx, int Ny, int i, double* x0, double* y0, double dy, 
		int rank, const char* filename,	int rank0, int Ny0, int Nym)  
{
    fstream myfile;
    myfile.open (filename, fstream::app);
    for (int j = Ny0; j < Nym; j++){
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
	if (rank == rank0) {
	//myfile.width(10); myfile.precision(3);
	myfile << *x0 << '\t' << *y0 << '\t' << s[i*Ny+j] << endl; 
	}
	*y0 += dy;
    } 
    myfile.close();
}

//Tiny funtion: Print one line data for one processer into file 
void Print_Ml	(double* s, int Nx, int Ny, int j, double* x0, double* y0, double dx, 
		int rank, const char* filename,	int rank0, int Nx0, int Nxm)  
{
    fstream myfile;
    myfile.open (filename, fstream::app);
    for (int i = Nx0; i < Nxm; i++){
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
	if (rank == rank0) {
	//myfile.width(10); myfile.precision(3);
	myfile << *x0 << '\t' << *y0 << '\t' << s[i*Ny+j] << endl; 
	}
	*x0 += dx;
    } 
    myfile.close();
}

//Tiny function(used in Combine_M): Print 3 blocks for 3 processers into file
void Print_M3	(double* s, int Nx, int Ny, double* x0, double dx, double dy,
		int rank, const char* filename, int Nx0, int Nxm, int k)
{
    double y0 = 0.0;
    //print into file
    for (int i = Nx0; i < Nxm; i++){					//rank 0 3 6
    	y0 = 0.0;
        Print_M(s, Nx, Ny, i, x0, &y0, dy, rank, filename, k, 0, Ny-1);  
        Print_M(s, Nx, Ny, i, x0, &y0, dy, rank, filename, 3+k, 1, Ny-1);
        Print_M(s, Nx, Ny, i, x0, &y0, dy, rank, filename, 6+k, 1, Ny);
	Print_endc (rank, filename);
    	*x0 += dx;
    } 

}

//Print the whole combined matrix for s v in file (use Print_M3)
void Combine_M 	(double* s, int Nx, int Ny, double dx, double dy, int rank, 
		const char* filename)  
{	
    //Initialize coordinate
    double x0 = 0.0;

    //Start print
    if (rank == 0) {
    	cout << "--------Writting into file " 
	<< filename << "----------" <<endl;
    for (int i=0; i<Nx*3-4; i++) {cout << ">";}				//progress bar
    cout << endl;
    }

    //print into file
    Print_M3 (s, Nx, Ny, &x0, dx, dy, rank, filename, 0, Nx-1, 0);	//rank 0 3 6
    Print_M3 (s, Nx, Ny, &x0, dx, dy, rank, filename, 1, Nx-1, 1);	//rank 1 4 7
    Print_M3 (s, Nx, Ny, &x0, dx, dy, rank, filename, 1, Nx, 2);	//rank 2 5 8

    //Finish print
    if (rank == 0) {
    	cout << endl << "--------Writting complete!----------" <<endl;
    }
}

//----------------------------vv and uv data functions----------------------------//

//Tiny funtion: Cal and print one colomn vv data for one processer into files 
void Print_uc	(double* s, int Nx, int Ny, int i, double* x0, double* y0,double dx, double dy, 
		int rank, const char* filename,	int rank0, int Ny0, int Nym)  
{
    fstream myfile;
    myfile.open (filename, fstream::app);
    for (int j = Ny0; j < Nym; j++){
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
	if (rank == rank0) {
	//myfile.width(10); myfile.precision(3);
	myfile << *x0 << '\t' << *y0 << '\t' << (s[i*Ny+j+1]-s[i*Ny+j-1])/(2*dy) << endl; 
	}
	*y0 += dy;
    } 
    myfile.close();
}

//Tiny funtion: Cal and print one line uv data for one processer into files 
void Print_vl	(double* s, int Nx, int Ny, int j, double* x0, double* y0, double dx, double dy, 
		int rank, const char* filename,	int rank0, int Nx0, int Nxm)  
{
    fstream myfile;
    myfile.open (filename, fstream::app);
    for (int i = Nx0; i < Nxm; i++){
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
	if (rank == rank0) {
	//myfile.width(10); myfile.precision(3);
	myfile << *x0 << '\t' << *y0 << '\t' << -(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(2*dx) << endl; 
	}
	*x0 += dx;
    } 
    myfile.close();
}

//Print one colume for uv in file
void Print_u	(double* s, int Nx, int Ny, double dx, double dy, int rank, 
		const char* filename)
{
    
    double y0 = dy;
    double x0 = 0.5;
    int     k = 1;
    int     i = (Nx-1)/2;

    fstream myfile;
    myfile.open (filename, fstream::app);
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
    if (rank == 0) {
    	myfile << "horizontal velocity" << endl;
   	myfile << "x y uv" << endl;
	myfile << "0.5 \t 0 \t 1" <<endl;
    }
    myfile.close();

    //print into file
        Print_uc(s, Nx, Ny, i, &x0, &y0, dx, dy, rank, filename, k, 1, Ny-1);  
        Print_uc(s, Nx, Ny, i, &x0, &y0, dx, dy, rank, filename, 3+k, 1, Ny-1);
        Print_uc(s, Nx, Ny, i, &x0, &y0, dx, dy, rank, filename, 6+k, 1, Ny-1);

    myfile.open (filename, fstream::app);
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
    if (rank == 0) {
	myfile << "0.5 \t 1 \t 0" <<endl;
    }
    myfile.close();

}

//Print one line for vv in file
void Print_v	(double* s, int Nx, int Ny, double dx, double dy, int rank, 
		const char* filename)
{
    
    double y0 = 0.5;
    double x0 = dx;
    int     k = 1;
    int     j = (Ny-1)/2;

    fstream myfile;
    myfile.open (filename, fstream::app);
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
    if (rank == 0) {
    	myfile << "vertical velocity" << endl;
   	myfile << "x y vv" << endl;
	myfile << "0 \t 0.5 \t 0" <<endl;
    }
    myfile.close();

    //print into file
        Print_vl(s, Nx, Ny, j, &x0, &y0, dx, dy, rank, filename, 3*k, 1, Nx-1);  
        Print_vl(s, Nx, Ny, j, &x0, &y0, dx, dy, rank, filename, 1+3*k, 1, Nx-1);
        Print_vl(s, Nx, Ny, j, &x0, &y0, dx, dy, rank, filename, 2+3*k, 1, Nx-1);

    myfile.open (filename, fstream::app);
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);
    if (rank == 0) {
	myfile << "1 \t 0.5 \t 0" <<endl;
    }
    myfile.close();

}

//-----------------------------Send_reciv functions-------------------------------//

//Swap s or v data between neighbouring domains
void Send_reciv (double* s, int Nx, int Ny, int rank, int size)
{	
    double* cx1 = new double[Nx-2];	//send array
    double* cx2 = new double[Nx-2];	//send array
    double* cy1 = new double[Ny-2];	//send array
    double* cy2 = new double[Ny-2];	//send array
    double* cx_r1= new double[Nx-2];	//recieve array
    double* cx_r2= new double[Nx-2];	//recieve array
    double* cy_r1= new double[Ny-2];	//recieve array
    double* cy_r2= new double[Ny-2];	//recieve array

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Request isreq, irreq;

    //data extraction
    switch (rank) {
    case 0:

	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}

    break;
    case 1:

	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}
	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}

    break;
    case 2:

	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}

    break;
    case 3:

	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}

    break;
    case 4:

	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}
	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}

    break;
    case 5:

	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int i=0; i<Nx-2; i++) {
	    cx2[i] = s[(i+1)*Ny+(Ny-2)];	//top
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}

    break;
    case 6:

	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}

    break;
    case 7:

	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int j=0; j<Ny-2; j++) {
	    cy2[j] = s[(Nx-2)*Ny+j+1];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}

    break;
    case 8:
	for (int j=0; j<Ny-2; j++) {
	    cy1[j] = s[1*Ny+j+1];		//left
	}
	for (int i=0; i<Nx-2; i++) {
	    cx1[i] = s[(i+1)*Ny+1];		//bottom
	}
    }

    //data swaping
    switch (rank) {
    case 0:
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD);			//right
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 3, 1, MPI_COMM_WORLD);        		//top
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 1, 2, MPI_COMM_WORLD, &isreq);	//right
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 3, 7, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 1:
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 0, 2, MPI_COMM_WORLD);			//left
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 2, 3, MPI_COMM_WORLD);			//right
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 4, 4, MPI_COMM_WORLD);			//top
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &isreq);  	//left   
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 2, 5, MPI_COMM_WORLD, &isreq);	//right
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 4, 10, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 2:
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 1, 5, MPI_COMM_WORLD);			//left
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 5, 6, MPI_COMM_WORLD);        		//top
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 1, 3, MPI_COMM_WORLD, &isreq);	//left
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 5, 14, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 3:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 0, 7, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 4, 8, MPI_COMM_WORLD);			//right
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 6, 9, MPI_COMM_WORLD);        		//top
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 4, 11, MPI_COMM_WORLD, &isreq);	//right
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 6, 17, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 4:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 1, 10, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 3, 11, MPI_COMM_WORLD);			//left
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 5, 12, MPI_COMM_WORLD);			//right
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 7, 13, MPI_COMM_WORLD);        		//top
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 1, 4, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 3, 8, MPI_COMM_WORLD, &isreq);	//left
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 5, 15, MPI_COMM_WORLD, &isreq);	//right
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 7, 19, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 5:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 2, 14, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 4, 15, MPI_COMM_WORLD);			//left
        MPI_Send(cx2, Nx-2, MPI_DOUBLE, 8, 16, MPI_COMM_WORLD);        		//top
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 2, 6, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 4, 12, MPI_COMM_WORLD, &isreq);	//left
        MPI_Irecv(cx_r2, Nx-2, MPI_DOUBLE, 8, 22, MPI_COMM_WORLD, &irreq);	//top

    break;
    case 6:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 3, 17, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 7, 18, MPI_COMM_WORLD);			//right
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 3, 9, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 7, 20, MPI_COMM_WORLD, &isreq);	//right

    break;
    case 7:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 4, 19, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 6, 20, MPI_COMM_WORLD);			//left
        MPI_Send(cy2, Ny-2, MPI_DOUBLE, 8, 21, MPI_COMM_WORLD);			//right
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 4, 13, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 6, 18, MPI_COMM_WORLD, &isreq);  	//left   
	MPI_Irecv(cy_r2, Ny-2, MPI_DOUBLE, 8, 23, MPI_COMM_WORLD, &isreq);	//right


    break;
    case 8:
        MPI_Send(cx1, Nx-2, MPI_DOUBLE, 5, 22, MPI_COMM_WORLD);        		//bottom
        MPI_Send(cy1, Ny-2, MPI_DOUBLE, 7, 23, MPI_COMM_WORLD);			//left
        MPI_Irecv(cx_r1, Nx-2, MPI_DOUBLE, 5, 16, MPI_COMM_WORLD, &irreq);	//bottom
	MPI_Irecv(cy_r1, Ny-2, MPI_DOUBLE, 7, 21, MPI_COMM_WORLD, &isreq);	//left

    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     	// Synchronise processes 

        MPI_Cancel(&irreq);
        MPI_Cancel(&isreq);

    //reconstruct matrices
    switch (rank) {
    case 0:

	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i] ;		//top
	}

    break;
    case 1:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i];		//top
	}
	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}

    break;
    case 2:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i];		//top
	}

    break;
    case 3:

	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i] ;		//top
	}

	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}

    break;
    case 4:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i] ;		//top
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}

    break;
    case 5:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+(Ny-1)] = cx_r2[i] ;		//top
	}

	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}

    break;
    case 6:

	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}

    break;
    case 7:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int j=0; j<Ny-2; j++) {
	    s[(Nx-1)*Ny+j+1] = cy_r2[j];		//right
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}

    break;
    case 8:

	for (int j=0; j<Ny-2; j++) {
	    s[0*Ny+j+1] = cy_r1[j];			//left
	}
	for (int i=0; i<Nx-2; i++) {
	    s[(i+1)*Ny+0] = cx_r1[i];			//bottom
	}
    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     	// Synchronise processes 

    delete[] cx1, cy1, cx_r1, cy_r1, cx2, cy2, cx_r2, cy_r2;
}

// Member functions & constructors for LidDrivenCavity ---------------------------//

LidDrivenCavity::LidDrivenCavity() {}

LidDrivenCavity::~LidDrivenCavity() {}

void LidDrivenCavity::SetDomaindxdy(double dx1, double dy1) {
    dx = dx1;
    dy = dy1;
}

void LidDrivenCavity::SetGridSize(int nx, int ny) {
    Nx = nx;
    Ny = ny;
}

void LidDrivenCavity::SetTimeStep(double deltat) {
    dt = deltat;
}


void LidDrivenCavity::SetReynoldsNumber(double re) {
    Re = re;
}

//combine all domains and print out v and s
void LidDrivenCavity::Combine_s (int rank, int size)  
{
    //print v
    string str = "data_v" + to_string((int)Re) + ".txt";
    char* filename = const_cast<char*>(str.c_str());

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    if(rank == 0) {
	cout << "Combining vorticity matrix" << endl; 
    }
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    Combine_M (v, Nx, Ny, dx, dy, rank, filename); 
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);  

    //print s
    string strx = "data_s" + to_string((int)Re) + ".txt";
    char* filenamex = const_cast<char*>(str.c_str());

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    if(rank == 0) {
	cout << "Combining streamfunction matrix" << endl; 
    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    Combine_M (s, Nx, Ny, dx, dy, rank, filenamex); 
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);  
}

//combine all domains and print out uv and vv
void LidDrivenCavity::Combine_v (int rank, int size)  
{
    //print uv	
    string str = "data_uv" + to_string((int)Re) + ".txt";
    char* filename = const_cast<char*>(str.c_str());

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    if(rank == 0) {
	cout << "Printing uv data" << endl; 
    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    Print_u (s, Nx, Ny, dx, dy, rank, filename);  
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 

    //print vv
    string strx = "data_vv_x" + to_string((int)Re) + ".txt";
    char* filenamex = const_cast<char*>(strx.c_str());

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    if(rank == 0) {
	cout << "Printing vv data" << endl; 
    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 
    Print_v (s, Nx, Ny, dx, dy, rank, filenamex);  
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD); 

}

//find the minimum value of s
void LidDrivenCavity::Find_min (int rank, int size)
{
    if(rank == 0) {
	cout << "Calculating mininum streamfunction" << endl; 
    }
    double mins = 0.0;
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);       // Synchronise processes 
    for (int i=0; i<9; i++) {
        if (rank == i) {
	    mins = *min_element(s,s+n);
	}
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);       // Synchronise processes 
        if (rank == i) {
	    cout << "mininum streamfunction in rank " ;
	    cout << i << " is: "<< mins << endl; 
        }
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);       // Synchronise processes 
    }
}

//Prior Calculation
void LidDrivenCavity::Prior_Cal()	
{
    n = Nx*Ny;
    n1 = (Nx-2)*(Ny-2);
    p4 = dx*dx;
    p5 = dy*dy;
    p1 = 2/p4+2/p5;
    p2 = -1/p5;
    p3 = -1/p4;

    kl = Ny-2;
    ku = Ny-2;
    ldab = 1+2*kl+ku;

    //prior set pointers
    v = new double[n];
    s = new double[n];
    s1= new double[n];
    s_sub = new double[n1];
    A = new double[ldab*n1];
    ipiv = new int [n1];
}

//Check the CFL number
int LidDrivenCavity::CFL_Check(int rank) 	
{
    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);       // Synchronise processes 
    if (dt >= Re*dx*dy/4.0) {
	if (rank == 0) {
	cout << "-------   Warning: too large dt   -------" << endl
	     << "----------- code will blow up -----------"<<endl 
	     << "-----------------------------------------" <<endl;
	}
	return 1;
	}
    else {
	if (rank == 0) {
	cout << "----------   CFL check passed   ----------" << endl
	     << "-----------------------------------------" <<endl;
	}
	return 0;
        }
}

//Construct A matrix and imply pre_factorazation
void LidDrivenCavity::Construct_A()	
{
    int info;

    for (int i = 0; i < Nx-2; i++){
	for (int j = 0; j < Ny-2; j++){
	    A[(i*(Ny-2)+j)*ldab+(kl)] = p2;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j+1)*ldab+(kl+ku-1)] = p3;
	    }			

	    A[(i*(Ny-2)+j)*ldab+(kl+ku)] = p1;

	    if (j<Ny-3) {
	    A[(i*(Ny-2)+j)*ldab+(kl+ku+1)] = p3;
	    }			
			
	    A[(i*(Ny-2)+j)*ldab+(2*kl+ku)] = p2;				
    	} 
    }

    F77NAME(dgbtrf)(n1, n1, kl, ku, A, ldab, ipiv, &info);

    if (info) {
        cout << "Failed to LU factorise matrix" << endl;
    }

   /*//print A
    cout << "matrix A:" << endl;
    for (int i = 0; i <ldab; ++i){
	for (int j = 0; j < n1; ++j){
	   cout.precision(3);
	    cout << A[j*ldab+i] << '\t' ; 
	} 
	cout << endl;
     }*/
}

//initialization
void LidDrivenCavity::Initialise()	
{
    //set entire doamin as 0
    for (int i = 0; i < Nx; i++){
	for (int j = 0; j < Ny; j++){
	    v[i*Ny+j] = 0; 
	    s[i*Ny+j] = 0; 
	} 
    }
}

//boundary conditions at time t
void LidDrivenCavity::BC_t(int rank)	
{
    switch (rank) {
    case 6:
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+Ny-1] = (s[i*Ny+Ny-1]-s[i*Ny+Ny-2])*2.0/p5-2.0*u/dy;   //top v
    	}
    	for (int j = 1; j < Ny-1; j++){
	    v[j] = (s[j]-s[1*Ny+j])*2.0/p4;  				  //left
    	}	
    break;
    case 7:
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+Ny-1] = (s[i*Ny+Ny-1]-s[i*Ny+Ny-2])*2.0/p5-2.0*u/dy;   //top v
    	}
    break;
    case 8:
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+Ny-1] = (s[i*Ny+Ny-1]-s[i*Ny+Ny-2])*2.0/p5-2.0*u/dy;   //top v
    	}
    	for (int j = 1; j < Ny-1; j++){
	    v[(Nx-1)*Ny+j] = (s[(Nx-1)*Ny+j]-s[(Nx-2)*Ny+j])*2.0/p4;	  //right
    	}
    break;
    case 3:
    	for (int j = 1; j < Ny-1; j++){
	    v[j] = (s[j]-s[1*Ny+j])*2.0/p4;  				  //left
    	}	
    break;
    case 5:
    	for (int j = 1; j < Ny-1; j++){
	    v[(Nx-1)*Ny+j] = (s[(Nx-1)*Ny+j]-s[(Nx-2)*Ny+j])*2.0/p4;	  //right
    	}
    break;
    case 0:
    	for (int j = 1; j < Ny-1; j++){
	    v[j] = (s[j]-s[1*Ny+j])*2.0/p4;  				  //left
    	}
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+0] = (s[i*Ny+0]-s[i*Ny+1])*2.0/p5;			  //bottom v
    	}
    break;
    case 1:
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+0] = (s[i*Ny+0]-s[i*Ny+1])*2.0/p5;			  //bottom v
    	}	
    break;
    case 2:
    	for (int j = 1; j < Ny-1; j++){
	    v[(Nx-1)*Ny+j] = (s[(Nx-1)*Ny+j]-s[(Nx-2)*Ny+j])*2.0/p4;	  //right
    	}
    	for (int i = 0; i < Nx; i++){
	    v[i*Ny+0] = (s[i*Ny+0]-s[i*Ny+1])*2.0/p5;			  //bottom v
    	}
    }
}

//interior vorticity at time t
void LidDrivenCavity::Vor_t()		
{
    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] = -(s[(i+1)*Ny+j]-2*s[i*Ny+j]+s[(i-1)*Ny+j])/p4
			-(s[i*Ny+(j+1)]-2*s[i*Ny+j]+s[i*Ny+(j-1)])/p5;
	} 
    } 
}

//interior vorticity at time t+dt	
void LidDrivenCavity::Vor_t_plus()	
{
    for (int i = 1; i < Nx-1; i++){
	for (int j = 1; j < Ny-1; j++){
	    v[i*Ny+j] += dt*(
			-(s[i*Ny+j+1]-s[i*Ny+j-1])*(v[(i+1)*Ny+j]-v[(i-1)*Ny+j])/(4*dx*dy)
			+(v[i*Ny+j+1]-v[i*Ny+j-1])*(s[(i+1)*Ny+j]-s[(i-1)*Ny+j])/(4*dx*dy)
			+1/Re*((v[(i+1)*Ny+j]-2*v[i*Ny+j]+v[(i-1)*Ny+j])/p4
			      +(v[i*Ny+(j+1)]-2*v[i*Ny+j]+v[i*Ny+(j-1)])/p5)
			);
	} 
    }
}

//streamfunction at time t+dt	
void LidDrivenCavity::Str_t_plus()	
{
    PoissonSolver* Psolver = new PoissonSolver();		//class PoissonSolver

    for (int i = 0; i < n; i++){					
    s1[i]= s[i];						//save s at time t
    }

    for (int i = 0; i < Nx-2; i++){				//substract s_sub
	for (int j = 0; j < Ny-2; j++){
	s_sub[i*(Ny-2)+j] = v[(i+1)*Ny+j+1];
	if (j == Ny-3) {s_sub[i*(Ny-2)+j] -= s[(i+1)*Ny+j+2]*p2;} //boundary conditions
	if (j == 0)    {s_sub[i*(Ny-2)+j] -= s[(i+1)*Ny+j]*p2;}
	if (i == Nx-3) {s_sub[i*(Ny-2)+j] -= s[(i+2)*Ny+j+1]*p3;}
	if (i == 0)    {s_sub[i*(Ny-2)+j] -= s[(i+0)*Ny+j+1]*p3;}
	}
    }

    Psolver->Get_para(n1, kl, ku, ldab, ipiv, A, s_sub);	//>> Psolver
    Psolver->Solve_L();						//./Psolver
    s_sub = Psolver->Give_para();				//<< Psolver

    for (int i = 0; i < Nx-2; i++){				//re_construct s with a new s_sub
	for (int j = 0; j < Ny-2; j++){
	 s[(i+1)*Ny+j+1] = s_sub[i*(Ny-2)+j];
	}
    }

}

//define convergance or divergence
int LidDrivenCavity::Def_Cov(int rank, int size)		
{
    double ds=0, ds_s=0;					//use to judge
    for (int i = 0; i < n; i++){
    s1[i]-= s[i];						//calculate s1
    }
    ds = F77NAME(dnrm2) (n, s1, 1);				//calculate ds
    ds = ds * ds;
    MPI_Allreduce(&ds, &ds_s, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    ds_s = sqrt(ds_s);

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     		// Synchronise processes 

   if (rank == 0) {
	cout << "ds" << " = " << ds_s << endl ;
        cout << "-----------------------------------------" <<endl;
    }

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     		// Synchronise processes 

    if (ds_s < 0.01) {
	if (rank == 0) {
	cout << "congrats converges" << endl
	     << "-----------------------------------------" <<endl;
	}
	return 1;
    }

    else if (ds_s > 1000) {
	if (rank == 0) {
	cout << "LOL U diverges" << endl
	     << "-----------------------------------------" <<endl;
	}
	return 1;
    }

    else {
    	MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     		// Synchronise processes 
	return 0;
   }
}

//Swap v_data from neighbouring domains
void LidDrivenCavity::Send_reciv_v (int rank, int size)
{	
    Send_reciv (v, Nx, Ny, rank, size);
}

//Swap s_data from neighbouring domains
void LidDrivenCavity::Send_reciv_s (int rank, int size)
{	
    Send_reciv (s, Nx, Ny, rank, size);
}

// Member functions & constructors for class PoissonSolver ---------------------------//

PoissonSolver::PoissonSolver() {}

PoissonSolver::~PoissonSolver() {}

//Pass parameters from LidDrivenCavity class
void PoissonSolver::Get_para(int n11, int kl1, int ku1, int ldab1,	
	          	     int* ipiv1, double* A1, double* s_sub1)	
{
    n1   = n11;
    kl   = kl1;
    ku   = ku1;
    ldab = ldab1;

    s_sub= new double[n1];
    A = new double[ldab*n1];
    ipiv = new int[n1];

    for(int i=0; i< ldab*n1; i++) {
	A[i] = A1[i];
    }

    for(int i=0; i< n1; i++) {
	ipiv[i] = ipiv1[i];
	s_sub[i] = s_sub1[i];
     }

}

//Solve poisson function after pre_factorization
void PoissonSolver::Solve_L()		
{
    F77NAME(dgbtrs)('N', n1, kl, ku, nrhs, A, ldab, ipiv, s_sub, n1, &info); //solve s at time t+dt

    if (info) {
        cout << "Failed to solve" << endl;
    }
}

//Solve poisson function with pre_factorization
double* PoissonSolver::Give_para()		
{
    return s_sub;
}

//--------------------------------------------------------------------------------//
//---------------------------------MAIN FUNCTION----------------------------------//
//--------------------------------------------------------------------------------//

int main(int argc, char** argv) 
{ 
    // MPI commands
    int    rank, size;
    MPI_Init(&argc, &argv);                 	// Initialise MPI
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);      	// Get the process index
    MPI_Comm_size(MPI_COMM_WORLD, &size);      	// Get total number of processes

    //define parameters
    double Lx = 0;	//Length of the domain in the x-direction
    double Ly = 0;	//Length of the domain in the y-direction
    int    Nx = 0;	//Number of grid points in x-direction.
    int    Ny = 0;	//Number of grid points in y-direction.
    int    Px = 1; 	//Number of partitions in the x-direction (parallel)
    int    Py = 1; 	//Number of partitions in the y-direction (parallel)
    double dt = 0;	//Time step size
    double T  = 0;	//Final time
    double Re = 0;	//Renolds number
    double t  = 0;	//Current time
    int timestep = 0;	//tiem step

    // Create a new instance of the LidDrivenCavity class
    LidDrivenCavity* solver = new LidDrivenCavity();

    //allocate parameters
    Lx = stod(argv[1]);
    Ly = stod(argv[2]);
    Px = stoi(argv[5]);
    Py = stoi(argv[6]);
    Nx = stoi(argv[3]);
    Ny = stoi(argv[4]);
    dt = stod(argv[7]);
    T  = stod(argv[8]);
    Re = stod(argv[9]);

    //Announcements
    Announce (argc, rank, size, Lx, Ly, Nx, Ny, Px, Py, dt, T, Re);

    //Check number of processes
    if (Check_P(rank, size, Px, Py)) { return 0; }

    //Alocate to class 
    solver->SetDomaindxdy(Lx/(Nx-1), Ly/(Ny-1));
    solver->SetGridSize((Nx-2)/Px+2, (Ny-2)/Py+2);	// Plus Halo edge
    solver->SetTimeStep(dt);
    solver->SetReynoldsNumber(Re);

    //preparation
    solver->Prior_Cal();
    if (solver->CFL_Check(rank)) {return 0;}    //CFL check

    //start solver
    solver->Construct_A();
    solver->Initialise();

    //first step

    timestep_p (rank, &timestep, &t, dt);

    solver->BC_t(rank);
    solver->Vor_t();
    solver->Send_reciv_v(rank, size);
    solver->Vor_t_plus();
    solver->Send_reciv_v(rank, size);
    solver->Str_t_plus();
    solver->Send_reciv_s(rank, size);
    solver->Def_Cov(rank, size);

    MPI_Barrier(MPI_Comm MPI_COMM_WORLD);     	// Synchronise processes 

    for (; t<T;) {

    timestep_p (rank, &timestep, &t, dt);

    solver->BC_t(rank);
    solver->Vor_t_plus();
    solver->Send_reciv_v(rank, size);
    solver->Str_t_plus();
    solver->Send_reciv_s(rank, size);

    if (solver->Def_Cov(rank, size)) {break;}

    }

    solver->Send_reciv_v(rank, size);
    solver->Send_reciv_s(rank, size);

    //results
    //solver->Combine_s (rank, size);
    solver->Combine_v (rank, size);
    //solver->Find_min (rank, size);

    MPI_Finalize();                         // Finished using MPI
    return 0;
}
