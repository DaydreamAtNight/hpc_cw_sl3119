#! /usr/bin/env python

#import matplotlib as mpl #use for remote server
#mpl.use('AGG')           #use for remote server

import numpy as np
import matplotlib.pyplot as plt
import sys

########################################################################
#
#  $Name: PLOT_MONITOR
#  $Description: PYTHON SCRIPT FOR PLOT THE MONITOR.DAT FILE AS INTERATING.
#  $Author: Songrui LI
#  $Date: March 2019
#
########################################################################

def open_point(filename):
	"""
	Open an monitor file.
	The header is extracted with the variables names, and the numerical data is transformed from string (text) to float numbers and stored into a NUMPY array.
	"""
	with open(filename,'r') as f:
		data = f.readlines()
	header = data[1].split('" "')
	data = data[2:]
	data = [line.split() for line in data]
	data = [map(float,line) for line in data]
        return header, np.array(data)

def set_axis(header):
        print header
        num_x = 2              #x axis variable number 1:thistime,2:inner-iter
        print '\ninput y axis variable number (started from 1)'
        num_y = 3
        return num_x, num_y

for i in range(4):
	filename = sys.argv[i+1]
	header, data = open_point(filename)
	num_x, num_y = set_axis(header)
	plt.plot(data[:,num_x-1], data[:,num_y-1], label=filename)

plt.title('horizontal velocity x=0.5')
plt.xlabel('y')
plt.ylabel('horizontal velocity')
plt.legend()
plt.show()
