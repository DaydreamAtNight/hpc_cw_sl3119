t=100

set terminal png
outfile = sprintf('data_s%.0f.png',t)
set output outfile
set xr[0:1] #0.025
set yr[0:1]  #0.15

set cbrange [-0.005:0]
#unset colorbox
set palette defined ( 0 "#000090",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70")
#set grid lw 2 lt 6
set xtics out
set ytics out
set pm3d map
set pm3d interpolate 0,0
set size 0.707,1
outfile = sprintf('data_s%.0f.txt',t)
splot outfile u 1:2:3
reset

set terminal png
outfile = sprintf('data_v%.0f.png',t)
set output outfile
set xr[0:1] #0.25
set yr[0:1]  #0.25
set cbrange [-4:4]
#unset colorbox
set palette defined ( 0 "#000090",\
                      1 "#000fff",\
                      2 "#0090ff",\
                      3 "#0fffee",\
                      4 "#90ff70",\
                      5 "#ffee00",\
                      6 "#ff7000",\
                      7 "#ee0000",\
                      8 "#7f0000")
#set grid lw 2 lt 6
set xtics out
set ytics out
set pm3d map
set pm3d interpolate 0,0
set size 0.707,1
outfile = sprintf('data_v%.0f.txt',t)
splot outfile u 1:2:3
reset

